<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{config('app.name')}}</title>
<link rel="stylesheet" href="{{mix('css/app.css')}}">
</head>
<body>
    <div class="app">
         
    <teste title="Aloa"></teste>
    {{-- exibição condicional --}}
    <teste v-show="visible()" title="Outro allgg"></teste>
    
        <example-component></example-component>   
    </div>

<script src="{{mix('js/app.js')}}"></script>
</body>
</html>