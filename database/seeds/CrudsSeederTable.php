<?php

use Illuminate\Database\Seeder;
use Illuminate\Http\Response;
use Faker\Factory as Faker;
use App\crud;

class CrudsSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i < 2; $i++) { 
            # code...
            $faker = Faker::create('App\crud');
            DB::table('cruds')->insert([
                'name' => $faker->lexify('????????'),
                'color' => $faker->boolean ? 'red': 'green',
            ]);
        }
       
    }

    /**
     * Comando para alimentar o banco com Faker
     * php artisan db:seed --class=CrudsSeederTable
     */
}
