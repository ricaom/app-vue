<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class crud extends Model
{
   public function up(){
       Schema::create('cruds', function (Blueprint $table) {
           $table->bigIncrements('id');
           $table->string('name');
           $table->string('color');
           
           $table->timestamps();
       });
   }
}
